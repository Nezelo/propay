<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

use App\Language;
use App\Interest;
use App\Mail\Mail\NotifyUserCreated;

/**
 *
 * @author Bongani.Mondlane
 *
 */
class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users.index')->with('users', User::all());
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $languages      = Language::all();
        $interests      = Interest::all();
        
        if($languages->count() == 0 || $interests->count() == 0)
        {
            Session::flash('info', 'You must have some languages and interests before attempting to create a user.');
            
            return redirect()->back();
        }
        
        return view('users.create')->with('languages', $languages)
                                   ->with('interests', $interests);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validates request data.
        $this->validate($request, [
            'name' => 'required|max:120',
            'surname'=> 'required|max:120',
            'email' => 'required|email|max:120|unique:users',
            'id_number' => 'required|sa_id|unique:profiles',
            'mobile_number' => 'required|numeric',
            'dob' => 'required|date_format:Y-m-d',
            'langauge_id' => 'required|numeric',
            'interests'=>'required'
        ]);
        
        try
        {
            //Begin db transaction
            DB::connection()->getPdo()->beginTransaction();
            
            $user = User::create([
                'name' => $request->name,
                'surname' => $request->surname,
                'email' => $request->email,
                'password' => bcrypt('password')
            ]);
            
            $profile = Profile::create([
                'user_id' => $user->id,
                'id_number'=> $request->id_number,
                'mobile_number'=> $request->mobile_number,
                'dob'=> $request->dob,
                'langauge_id'=> $request->langauge_id,
            ]);
            
            //Attach interest
            $profile->interests()->attach($request->interests);
            
            //Commit db transactions 
            DB::connection()->getPdo()->commit();
            
            //Sent email to the created user
            $notifyUserEmail = new NotifyUserCreated($user);
            $notifyUserEmail->from('bongani.propay@gmail.com', 'ProPay Admin');
            $notifyUserEmail->subject('User Acccount Created');
            Mail::to($request->email)->send($notifyUserEmail);
        }
        catch (\PDOException $e)
        {
            DB::connection()->getPdo()->rollBack();
            Session::flash('error', 'Could not create user something went wrong please contact your admistrator.');
            return;
        }
        
        Session::flash('success', 'User added successfully.');
        
        return redirect()->back();
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($user)
    {
        $user = User::find($user);
        
        return view('users.edit')->with('user', $user)
                                 ->with('languages', Language::all())
                                 ->with('interests', Interest::all());
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user)
    {
        //Validates request data.
        $this->validate($request, [
            'name' => 'required|max:120',
            'surname'=> 'required|max:120',
            'id_number' => 'required|sa_id|unique:profiles',
            'mobile_number' => 'required|numeric',
            'dob' => 'required|date_format:Y-m-d|sa_id_dob',
            'langauge_id' => 'required',
            'interests'=>'required'
        ]);
        
        try 
        {
            //Begin db transaction
            DB::connection()->getPdo()->beginTransaction();
            
            $user = User::find($user);
            $user->name         = $request->name;
            $user->surname      = $request->surname;
            $user->save();
            
            $profile = Profile::where('user_id', $user->id)->first();
            $profile->id_number = $request->id_number;
            $profile->mobile_number = $request->mobile_number;
            $profile->dob = $request->dob;
            $profile->langauge_id = $request->langauge_id;
            
            //Save user and sync interests.
            $profile->save();
            $profile->interests()->sync($request->interests);
            
            //Commit db transactions.
            DB::connection()->getPdo()->commit();
        }
        catch (\PDOException $e)
        {
            DB::connection()->getPdo()->rollBack();
            Session::flash('error', 'Could not update user something went wrong please contact your admistrator.');
            return;
        }

        Session::flash('success', 'User updated successfully.');
        
        return redirect()->back();
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($user)
    {
        //Finds and temporally deletes a user.
        $userToTrash = User::find($user);
        $userToTrash->delete();
        
        Session::flash('success', 'User trashed.');
        
        return redirect()->back();
    }
    
    /**
     * Deletes the specified resource from storage.
     *
     * @param  int  $user
     * @return \Illuminate\Http\Response
     */
    public function delete($user)
    {
        //Finds and permanately deletes a user.
        $userToDelete = User::withTrashed()->where('id', $user)->first();
        $userToDelete->profile->interests()->sync([]);
        $userToDelete->profile->delete();
        $userToDelete->forceDelete();
        
        Session::flash('success', 'User deleted permanently..');
        
        return redirect()->back();
    }
    
    /**
     * Get trashed users.
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function trashed()
    {
        $users = User::onlyTrashed()->get();
        
        return view('users.trashed')->with('users', $users);
    }
    
    /**
     * Restores the specified resource from storage.
     *
     * @param  int  $user
     * @return \Illuminate\Http\Response
     */
    public function restore($user)
    {
        //Get the trashed user.
        $userToRestore = User::withTrashed()->where('id', $user)->first();
        $userToRestore->restore();
        
        Session::flash('success', 'User restored.');
        
        return redirect()->route('users.index');
    }
    
    /**
     * Assign user to be an admin
     *
     * @param integer $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function admin($id)
    {
        $user = User::find($id);
        $user->admin = 1;
        $user->save();
        
        Session::flash('success', 'Successfully changed user permissions.');
        
        return redirect()->back();
    }
    
    /**
     * Deadmin a user
     *
     * @param integer $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function notAdmin($id)
    {
        $user = User::find($id);
        $user->admin = 0;
        $user->save();
        
        Session::flash('success', 'Successfully changed user permissions.');
        
        return redirect()->back();
    }
    
}
