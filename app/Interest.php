<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * 
 * @author Bongani.Mondlane
 *
 */
class Interest extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];
    
    /**
     * Interest and Profile relationship.
     * An interest belongs to many profiles.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function profiles()
    {
        return $this->belongsToMany('App\Profile');
    }
}
