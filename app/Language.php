<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * 
 * @author Bongani.Mondlane
 *
 */
class Language extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];
    
    /**
     * Category and Post relationship.
     * A category has many posts.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany('App\Post');
    }
}
