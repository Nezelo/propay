<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * 
 * @author Bongani.Mondlane
 *
 */
class Profile extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'id_number', 'mobile_number', 'dob', 'langauge_id',
    ];
    
    /**
     * Profile and User relationship.
     * A profile belongs to a single user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    /**
     * Profile and Language relationship.
     * A single Profile belongs to a single language.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('App\Language');
    }
    
    /**
     * Profile and Interest relationship.
     * A Profile belongs to many Interests.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function interests()
    {
        return $this->belongsToMany('App\Interest');
    }
}
