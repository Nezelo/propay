<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use DateTime;

class AppServiceProvider extends ServiceProvider
{
    private $dob;
    
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        
        //Phone number validation rule
        Validator::extend('sa_id', function($attribute, $value, $parameters)
        {
            $match = preg_match("!^(\d{2})(\d{2})(\d{2})\d\d{6}$!", $value, $matches);
            if (!$match) {
                return false;
            }
            
            list (, $year, $month, $day) = $matches;
            
            /**
             * Check that the date is valid
             */
            if (!date('%y-%m-%d', strtotime("$year-$month-$day"))) {
                
                return false;
            }else {
                $this->dob = "$year-$month-$day";
            }
            
            $this->dob = "$year-$month-$day";
            
            /**
             * Check citizenship of the users id (0 = .za, 1 = other)
             */
            if (!in_array($value{10}, array(0, 1))) {
                return false;
            }
            /**
             * 'A' digit
             */
            if (!in_array($value{11}, array(8, 9))) {
                return false;
            }
            
            return true;
        });
        
        //South African Id number message.
        Validator::replacer('sa_id', function($message, $attribute, $rule, $parameters) {
            return str_replace(':attribute',$attribute, 'Id number is invalid, it must a South African Id number.');
        });
        
        //Check if the DOB is the same as on the ID number
        Validator::extend('sa_id_dob', function($attribute, $value, $parameters)
        {
            /**
             * Check that the date is valid
             */
            if (date('y-m-d', strtotime($value)) !== $this->dob) {
                
                return false;
            }
            
            return true;
        });
        
        //South African DOB number message.
        Validator::replacer('sa_id_dob', function($message, $attribute, $rule, $parameters) {
            return str_replace(':attribute',$attribute, 'The date of birth is not the same as on the Id number');
        });
            
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
