<?php

use Illuminate\Database\Seeder;
use App\Interest;

class InterestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $interest1 = ['name' => 'Technology'];
        $interest2 = ['name' => 'Software Development'];
        $interest3 = ['name' => 'Politics'];
        $interest4 = ['name' => 'Humanity'];
        $interest5 = ['name' => 'Health'];
        $interest6 = ['name' => 'Agriculture'];
        $interest7 = ['name' => 'Business'];
        $interest8 = ['name' => 'Clothing'];
        
        Interest::create($interest1);
        Interest::create($interest2);
        Interest::create($interest3);
        Interest::create($interest4);
        Interest::create($interest5);
        Interest::create($interest6);
        Interest::create($interest7);
        Interest::create($interest8);
    }
}
