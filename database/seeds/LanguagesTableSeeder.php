<?php

use Illuminate\Database\Seeder;
use App\Language;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $language1 = ['name' => 'Siswati'];
        $language2 = ['name' => 'English'];
        $language3 = ['name' => 'Sepedi'];
        $language4 = ['name' => 'Sesotho'];
        $language5 = ['name' => 'Tsonga'];
        $language6 = ['name' => 'Zulu'];
        $language7 = ['name' => 'Afrikans'];
        $language8 = ['name' => 'Ndebele'];

        Language::create($language1);
        Language::create($language2);
        Language::create($language3);
        Language::create($language4);
        Language::create($language5);
        Language::create($language6);
        Language::create($language7);
        Language::create($language8);
    }
}
