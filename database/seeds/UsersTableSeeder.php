<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'name' => 'admin',
            'surname' => 'admin',
            'password' => bcrypt('admin'),
            'email' => 'admin@test.com',
            'admin' => 1
        ]);
    }
}
