<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


</head>
<body>

	<p>Hi {{$user->name }},</p>
	
	<p>Thank You for choosing us. Your account is created!</p>
	
	<p>Kind Regards<br> ProPay</p>
</body>
