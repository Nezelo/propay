@extends('layouts.app')

@section('content')

	<div class="panel panel-default">
	
		<div class="panel-heading">
			<h3>Create a new user</h3>
		</div>
		
		<div class="panel-body">
		
			<form action="{{ route('users.store') }}" method="POST">
				{{ csrf_field() }}
				
				<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
					<label for="name">Name</label>
					<input type="text" name="name" value="{{ old('name') }}" id="name" class="form-control">
					@if ($errors->has("name"))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
				</div>
				
				<div class="form-group {{ $errors->has('surname') ? ' has-error' : '' }}">
					<label for="surname">Surname</label>
					<input type="text" name="surname"  value="{{ old('surname') }}" class="form-control">
					@if ($errors->has("surname"))
                        <span class="help-block">
                            <strong>{{ $errors->first('surname') }}</strong>
                        </span>
                    @endif
				</div>
				
				<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
					<label for="name">Email</label>
					<input type="email" name="email" value="{{ old('email') }}" id="email" class="form-control">
					@if ($errors->has("email"))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
				</div>
				
				<div class="form-group {{ $errors->has('id_number') ? ' has-error' : '' }}">
					<label for="id_number">Id Number</label>
					<input type="text" name="id_number" value="{{ old('id_number') }}" value="id_number" id="id_number" class="form-control">
					@if ($errors->has("id_number"))
                        <span class="help-block">
                            <strong>{{ $errors->first('id_number') }}</strong>
                        </span>
                    @endif
				</div>
				
				<div class="form-group {{ $errors->has('mobile_number') ? ' has-error' : '' }}">
					<label for="mobile_number">Mobile Number</label>
					<input type="text" name="mobile_number" value="{{ old('mobile_number') }}" value="mobile_number" id="mobile_number" class="form-control">
					@if ($errors->has("mobile_number"))
                        <span class="help-block">
                            <strong>{{ $errors->first('mobile_number') }}</strong>
                        </span>
                    @endif
				</div>
				
				<div class="form-group {{ $errors->has('dob') ? ' has-error' : '' }}">
					<label for="dob">Date Of Birth</label>
					<input type="date" name="dob" id="dob" class="form-control">
					@if ($errors->has("dob"))
                        <span class="help-block">
                            <strong>{{ $errors->first('dob') }}</strong>
                        </span>
                    @endif
				</div>
				
				<div class="form-group {{ $errors->has('langauge_id') ? ' has-error' : '' }}">
					<label for="langauge">Select Language</label>
					<select name="langauge_id" id="languge" class="form-control cols-m-4">
						@foreach($languages as $langauge)
							<option value="{{ $langauge->id }}">{{ $langauge->name }}</option>
						@endforeach
					</select>
					@if ($errors->has('langauge_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('langauge_id') }}</strong>
                        </span>
                    @endif
				</div>
				
				<div class="form-group">
					<label for="interests">Select Interests</label>
					@foreach ($interests as $interest)
					
                    	<div class="checkbox">
                            <label><input type="checkbox" name="interests[]" value="{{ $interest->id }}"> {{ $interest->name }}</label>
                        </div>
                    
                    @endforeach
				</div>
				
				
				<div class="form-group">
					<div class="text-center">
						<button class="btn btn-primary" type="submit">Add User</button>
					</div>
				</div>
				
				
			</form>
		</div>
	
	</div>
@stop