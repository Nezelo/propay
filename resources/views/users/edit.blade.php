@extends('layouts.app')

@section('content')

	<div class="panel panel-default">
	
		<div class="panel-heading">
			<h3>Edit: {{ $user->name }}</h3>
		</div>
		
		<div class="panel-body">
		
			<form action="{{ route('users.update', ['user'=> $user->id]) }}" method="POST">
				{{ csrf_field() }}
				{{ method_field('PUT') }}
				
				<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
					<label for="name">Name</label>
					<input type="text" name="name" value="{{ $user->name }}" id="name" class="form-control">
					@if ($errors->has("name"))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
				</div>
				
				<div class="form-group {{ $errors->has('surname') ? ' has-error' : '' }}">
					<label for="surname">Surname</label>
					<input type="text" name="surname" value="{{ $user->surname }}" class="form-control">
					@if ($errors->has("surname"))
                        <span class="help-block">
                            <strong>{{ $errors->first('surname') }}</strong>
                        </span>
                    @endif
				</div>
				
				<div class="form-group {{ $errors->has('id_number') ? ' has-error' : '' }}">
					<label for="id_number">Id Number</label>
					<input type="text" name="id_number" value="{{ $user->profile->id_number }}" id="id_number" class="form-control">
					@if ($errors->has("id_number"))
                        <span class="help-block">
                            <strong>{{ $errors->first('id_number') }}</strong>
                        </span>
                    @endif
				</div>
				
				<div class="form-group {{ $errors->has('mobile_number') ? ' has-error' : '' }}">
					<label for="mobile_number">Mobile Number</label>
					<input type="text" name="mobile_number" value="{{ $user->profile->mobile_number }}" id="mobile_number" class="form-control">
					@if ($errors->has("mobile_number"))
                        <span class="help-block">
                            <strong>{{ $errors->first('mobile_number') }}</strong>
                        </span>
                    @endif
				</div>
				
				<div class="form-group {{ $errors->has('dob') ? ' has-error' : '' }}">
					<label for="dob">Date Of Birth</label>
					<input type="date" name="dob" value="{{ $user->profile->dob }}"  id="dob" class="form-control">
					@if ($errors->has("dob"))
                        <span class="help-block">
                            <strong>{{ $errors->first('dob') }}</strong>
                        </span>
                    @endif
				</div>
				
				<div class="form-group {{ $errors->has('langauge_id') ? ' has-error' : '' }}">
					<label for="language">Select Language</label>
					<select name="langauge_id" id="language" class="form-control cols-m-4">
						@foreach($languages as $language)
							<option value="{{ $language->id }}"
							
							@if ($user->profile->langauge_id === $language->id)
								selected
							@endif
							>{{ $language->name }}</option>
						@endforeach
					</select>
					@if ($errors->has('langauge_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('langauge_id') }}</strong>
                        </span>
                    @endif
				</div>
				
				
				<div class="form-group">
					<label for="interest">Select Interests</label>
					@foreach ($interests as $interest)
					
                    	<div class="checkbox">
                            <label><input type="checkbox" name="interests[]" value="{{ $interest->id }}"
                            	@foreach ($user->profile->interests as $user_interest)
                            		@if ($interest->id == $user_interest->id)
                            			checked
                            		@endif
                            	@endforeach
                            
                            > {{ $interest->name }}</label>
                        </div>
                    @endforeach
				</div>
				
				<div class="form-group">
					<div class="text-center">
						<button class="btn btn-primary" type="submit">Update User</button>
					</div>
				</div>
				
			</form>
		</div>
	
	</div>
@stop