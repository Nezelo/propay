@extends('layouts.app')

@section('content')

<div class="panel panel-default">
	<div class="panel-heading">
		Users
	</div>
	<div class="panel-body">
	    <table class="table table-hover">
    	<thead>
    		<th>
    			Name
    		</th>
    		<th>
    			Surname
    		</th>
    		<th>
    			Email
    		</th>
    		<th>
    			Edit
    		</th>
    		<th>
    			Delete
    		</th>
    	</thead>
    	
    	<tbody>
        	@if ($users->count() > 0)
        		@foreach ($users as $user)
        			<tr>
        				<td>{{ $user->name }}</td>
        				<td>{{ $user->surname }}</td>
        				<td>{{ $user->email }}</td>
        				<td><a href="{{ route('users.edit', ['user'=> $user->id]) }}" class="btn btn-xs btn-info">Edit</a></td>
    					@if (Auth::id() !== $user->id)
        					<td>
        						<form action="{{ route('users.destroy', ['user'=> $user->id]) }}" method="post">
            						{{ csrf_field() }}
            						{{ method_field('DELETE') }}
            						<button class="btn btn-xs btn-danger" type="submit">Trash</button>
            					</form>
            				</td>
    					@endif
        			</tr>
        		@endforeach
        	@else
        		<tr>
        			<th colspan="5" class="text-center">No Users</th>
        		</tr>
        	@endif
    		
    	</tbody>
    </table>
	</div>

</div>



@endsection