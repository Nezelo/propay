@extends('layouts.app')

@section('content')

	@if(count($errors) > 0)
	@endif
	<div class="panel panel-default">
	
		<div class="panel-heading">
			Edit your profile
		</div>
		
		<div class="panel-body">
		
			<form action="{{ route('user.profile.update')}}" method="POST" enctype="multipart/form-data">
				{{ csrf_field() }}
				
				<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
					<label for="name">Name</label>
					<input  type="text" name="name" class="form-control" value="{{ $user->name }}">
					@if ($errors->has('"name"'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
				</div>
				
				
				<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
					<label for="email">Email</label>
					<input  type="email" name="email" class="form-control" value="{{ $user->email }}">
					@if ($errors->has('"email"'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
				</div>
				
				
				<div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
					<label for="password">New Password</label>
					<input type="password" name="password" class="form-control">
					@if ($errors->has('"password"'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
				</div>
				
				
				<div class="form-group {{ $errors->has('avatar') ? ' has-error' : '' }}">
					<label for="avatar">Upload Profile Picture</label>
					<input type="file" name="avatar" class="form-control">
					@if ($errors->has('"avatar"'))
                        <span class="help-block">
                            <strong>{{ $errors->first('avatar') }}</strong>
                        </span>
                    @endif
				</div>
				
				
				<div class="form-group {{ $errors->has('facebook') ? ' has-error' : '' }}">
					<label for="facebook">Facebook Profile</label>
					<input type="text" name="facebook" class="form-control" value="{{ $user->profile->facebook }}">
					@if ($errors->has('"facebook"'))
                        <span class="help-block">
                            <strong>{{ $errors->first('facebook') }}</strong>
                        </span>
                    @endif
				</div>
				
				
				<div class="form-group {{ $errors->has('youtube') ? ' has-error' : '' }}">
					<label for="youtube">Youtube Profile</label>
					<input type="text" name="youtube" class="form-control" value="{{ $user->profile->youtube }}">
					@if ($errors->has('"youtube"'))
                        <span class="help-block">
                            <strong>{{ $errors->first('youtube') }}</strong>
                        </span>
                    @endif
				</div>
				
				
				<div class="form-group {{ $errors->has('about') ? ' has-error' : '' }}">
					<label for="about">About</label>
					<textarea name="about" id="about" cols="6" rows="6" class="form-control" >{{ $user->profile->about }}</textarea>
					@if ($errors->has('"about"'))
                        <span class="help-block">
                            <strong>{{ $errors->first('about') }}</strong>
                        </span>
                    @endif
				</div>
				
				<div class="form-group">
					<div class="text-center">
						<button class="btn btn-success" type="submit">Update profile</button>
					</div>
				</div>
				
				
			</form>
		</div>
	
	</div>
@stop