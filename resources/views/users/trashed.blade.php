@extends('layouts.app')

@section('content')

<div class="panel panel-default">
	<div class="panel-heading">
		<h3>Trashed Users</h13>
	</div>
	<div class="panel-body">
	    <table class="table table-hover">
    	<thead>
    		<th>
    			Name
    		</th>
    		<th>
    			Surname
    		</th>
    		<th>
    			Email
    		</th>
    		<th>
    			Restore
    		</th>
    		<th>
    			Delete
    		</th>
    	</thead>
    	
    	<tbody>
        	@if ($users->count() > 0)
        		@foreach ($users as $user)
        			<tr>
        				<td>{{ $user->name }}</td>
        				<td>{{ $user->surname }}</td>
        				<td>{{ $user->email }}</td>
        				<td><a href="{{ route('user.restore', ['user'=> $user->id]) }}" class="btn btn-xs btn-success">Restore</a></td>
						<td><a href="{{ route('user.delete', ['user'=> $user->id]) }}" class="btn btn-xs btn-danger">Delete</a></td>
        			</tr>
        		@endforeach
        	@else
        		<tr>
        			<th colspan="5" class="text-center">No Users</th>
        		</tr>
        	@endif
    		
    	</tbody>
    </table>
	</div>

</div>



@endsection