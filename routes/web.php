<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});
    
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
    
//Admin group.
Route::group(['middleware'=>'auth'], function(){
    Route::resource('users', 'UsersController');
    
    Route::get('/user/restore/{user}', [
        
        'uses' => 'UsersController@restore',
        'as' => 'user.restore'
    ]);
    
    Route::get('/user/delete/{user}', [
        
        'uses' => 'UsersController@delete',
        'as' => 'user.delete'
    ]);
    
    Route::get('/trashed/users', [
        
        'uses' => 'UsersController@trashed',
        'as' => 'trashed.users'
    ]);
});
    


